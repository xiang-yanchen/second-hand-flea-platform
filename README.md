# 二手跳蚤平台

#### 介绍
uniapp-golang

#### 软件架构
软件架构说明


#### 安装教程

1.  uniapp 使用了uniCloud-aliyun，请自行前往uniCloud 开通并创建云服务空间-可以申请免费的
2.  golang_api 下的/config/app.yaml 修改服务器配置,请将配置信息改为自己的
3.  tiaozao 目录下的config.json 修改服务端连接地址
4. sql /golang_api/tiaozao.sql 导入数据库
#### 使用说明

1.  前端使用的uniapp框架，后端已使用golang重构
2.  web --- tiaozao
3.  server --- golang_api
4.  数据库 --- mysql


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


